package com.tompai.mc;

import java.util.ArrayList;
import java.util.List;

import com.tompai.printer.Printer;
import com.tompai.server.ctl.KLPanCtl;
import com.tompai.server.enumeration.LogLevel;
import com.tompai.server.enumeration.VCLevel;
import com.tompai.server.pojo.ExtendStores;
import com.tompai.server.pojo.ServerSetting;
import com.tompai.server.util.ConfigureReader;
import com.tompai.server.util.ServerTimeUtil;
import com.tompai.ui.callback.GetServerStatus;
import com.tompai.ui.callback.UpdateSetting;
import com.tompai.ui.module.ServerUIModule;
import com.tompai.ui.pojo.FileSystemPath;

/**
 * 
 * <h2>UI界面模式启动器</h2>
 * <p>
 * 该启动器将以界面模式启动klpan，请执行静态build()方法开启界面并初始化klpan服务器引擎。
 * </p>
 * 
 * @author tompai
 * @version 1.0
 */
public class UIRunner {

	private static UIRunner ui;

	private UIRunner() {
		Printer.init(true);
		final ServerUIModule ui = ServerUIModule.getInsatnce();
		KLPanCtl ctl = new KLPanCtl();// 服务器控制层，用于连接UI与服务器内核
		ServerUIModule.setStartServer(() -> ctl.start());
		ServerUIModule.setOnCloseServer(() -> ctl.stop());
		ServerUIModule.setGetServerTime(() -> ServerTimeUtil.getServerTime());
		ServerUIModule.setGetServerStatus(new GetServerStatus() {
			@Override
			public boolean getServerStatus() {
				// TODO 自动生成的方法存根
				return ctl.started();
			}
			@Override
			public int getPropertiesStatus() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getPropertiesStatus();
			}
			@Override
			public int getPort() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getPort();
			}
			@Override
			public boolean getMustLogin() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().mustLogin();
			}
			@Override
			public LogLevel getLogLevel() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getLogLevel();
			}
			@Override
			public String getFileSystemPath() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getFileSystemPath();
			}
			@Override
			public int getBufferSize() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getBuffSize();
			}
			@Override
			public VCLevel getVCLevel() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getVCLevel();
			}
			@Override
			public List<FileSystemPath> getExtendStores() {
				List<FileSystemPath> fsps = new ArrayList<FileSystemPath>();
				for (ExtendStores es : ConfigureReader.instance().getExtendStores()) {
					FileSystemPath fsp = new FileSystemPath();
					fsp.setIndex(es.getIndex());
					fsp.setPath(es.getPath());
					fsp.setType(FileSystemPath.EXTEND_STORES_NAME);
					fsps.add(fsp);
				}
				return fsps;
			}
			@Override
			public LogLevel getInitLogLevel() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getInitLogLevel();
			}
			@Override
			public VCLevel getInitVCLevel() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getInitVCLevel();
			}
			@Override
			public String getInitFileSystemPath() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getInitFileSystemPath();
			}
			@Override
			public String getInitProt() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getInitPort();
			}
			@Override
			public String getInitBufferSize() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getInitBuffSize();
			}
			@Override
			public boolean isAllowChangePassword() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().isAllowChangePassword();
			}
			@Override
			public boolean isOpenFileChain() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().isOpenFileChain();
			}
			@Override
			public int getMaxExtendStoresNum() {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().getMaxExtendstoresNum();
			}
		});
		ServerUIModule.setUpdateSetting(new UpdateSetting() {

			@Override
			public boolean update(ServerSetting s) {
				// TODO 自动生成的方法存根
				return ConfigureReader.instance().doUpdate(s);
			}
		});
		ui.show();
	}

	/**
	 * 
	 * <h2>以UI模式运行klpan</h2>
	 * <p>
	 * 启动UI模式操作并初始化服务器引擎，该方法将返回本启动器的唯一实例。
	 * </p>
	 * 
	 * @author tompai
	 * @return com.tompai.mc.UIRunner 本启动器唯一实例
	 */
	public static UIRunner build() {
		if (UIRunner.ui == null) {
			UIRunner.ui = new UIRunner();
		}
		return UIRunner.ui;
	}
}
