package com.tompai.server.enumeration;

public enum VCLevel {
	Standard, 
	Simplified, 
	Close;
}
