package com.tompai.server.enumeration;

public enum LogLevel {
	None, 
	Runtime_Exception, 
	Event;
}
