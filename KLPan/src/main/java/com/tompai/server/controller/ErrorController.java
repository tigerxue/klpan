package com.tompai.server.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tompai.printer.Printer;
import com.tompai.server.util.FileBlockUtil;
import com.tompai.server.util.LogUtil;

@ControllerAdvice
public class ErrorController {
	@Resource
	private FileBlockUtil fbu;
	@Resource
	private LogUtil lu;

	@ExceptionHandler({ Exception.class })
	public void handleException(final Exception e) {
		this.lu.writeException(e);
		this.fbu.checkFileBlocks();
		Printer.instance
				.print("处理请求时发生错误：\\n\\r------信息------\\n\\r"
						+ e.getMessage() + "\\n\\r------信息------");
	}
}
