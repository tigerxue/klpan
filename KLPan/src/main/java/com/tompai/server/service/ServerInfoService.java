package com.tompai.server.service;

public interface ServerInfoService {
	String getOSName();

	String getServerTime();
}
