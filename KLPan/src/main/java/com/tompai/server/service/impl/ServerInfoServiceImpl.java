package com.tompai.server.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.tompai.server.service.ServerInfoService;

@Service
public class ServerInfoServiceImpl implements ServerInfoService {
	@Override
	public String getOSName() {
		return System.getProperty("os.name");
	}

	@Override
	public String getServerTime() {
		final Date d = new Date();
		final DateFormat df = new SimpleDateFormat("YYYY年MM月dd日 hh:mm");
		return df.format(d);
	}
}
