package com.tompai.util.file.manager.pojo;

import java.util.List;

import com.tompai.server.model.Node;

/**
 * 
 * <h2>文件视图（文件管理器专用）</h2>
 * <p>
 * 该POJO用于封装需要显示给用户的文件夹内容，供文件管理器使用。提供：当前浏览的文件夹对象、本路径下所有文件夹和所有文件。
 * </p>
 * 
 * @author tompai
 * @version 1.0
 */
public class FolderView {

	private FsmFolder current;// 当前文件夹

	private List<FsmFolder> fsmFolders;// 该文件夹内所有文件夹（嵌套的，以便于依次加载）
	private List<Node> files;// 该文件夹内所有文件

	public FsmFolder getCurrent() {
		return current;
	}

	public void setCurrent(FsmFolder current) {
		this.current = current;
	}

	public List<FsmFolder> getFolders() {
		return fsmFolders;
	}

	public void setFolders(List<FsmFolder> fsmFolders) {
		this.fsmFolders = fsmFolders;
	}

	public List<Node> getFiles() {
		return files;
	}

	public void setFiles(List<Node> files) {
		this.files = files;
	}

}
