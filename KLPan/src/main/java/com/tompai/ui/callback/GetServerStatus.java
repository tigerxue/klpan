package com.tompai.ui.callback;

import java.util.List;

import com.tompai.server.enumeration.LogLevel;
import com.tompai.server.enumeration.VCLevel;
import com.tompai.ui.pojo.FileSystemPath;

public interface GetServerStatus {
	int getPropertiesStatus();

	boolean getServerStatus();

	int getPort();

	String getInitProt();

	int getBufferSize();

	String getInitBufferSize();

	LogLevel getLogLevel();

	LogLevel getInitLogLevel();

	VCLevel getVCLevel();

	VCLevel getInitVCLevel();

	String getFileSystemPath();

	String getInitFileSystemPath();

	boolean getMustLogin();

	boolean isAllowChangePassword();

	boolean isOpenFileChain();

	List<FileSystemPath> getExtendStores();

	int getMaxExtendStoresNum();
}
