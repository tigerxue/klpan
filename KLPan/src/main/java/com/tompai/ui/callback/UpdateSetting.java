package com.tompai.ui.callback;

import com.tompai.server.pojo.ServerSetting;

public interface UpdateSetting {
	boolean update(final ServerSetting s);
}
