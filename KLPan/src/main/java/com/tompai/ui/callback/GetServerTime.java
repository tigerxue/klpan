package com.tompai.ui.callback;

import java.util.Date;

public interface GetServerTime {
	Date get();
}
